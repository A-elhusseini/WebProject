<?php $this->assign('title', 'Ajouter un entrainement');?>
<?php session_start(); ?>
<div class="workout form">
<?php echo $this->Form->create('Workout'); ?>
    <fieldset>
        <legend><?php echo __('Add Workout'); ?></legend>
        <?php echo $this->Form->input('member id');
        echo $this->Form->input('sport');
        echo $this->Form->input('date'); 
        echo $this->Form->input('end date'); 
        echo $this->Form->input('description');        
        echo $this->Form->input('location');
        echo $this->Form->input('contest id'); 
       
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
